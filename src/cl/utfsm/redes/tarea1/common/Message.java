package cl.utfsm.redes.tarea1.common;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;


public class Message {
    private static final JSONParser parser = new JSONParser();

    private String type;
    private String value;

    public static String BYE_BYE = "ByeBye";
    public static String BLOCK_NOT_FOUND = "BlockUnavailable";
    public static String FILESYNC_WRAPPER = "FileSync";
    public static String OK = "Ok";
    public static String REVERSE_ROLES = "ReverseRoles";
    public static String SET_BLOCKSIZE = "SetBlockSize";


    public Message() {
        this.type = "";
        this.value = "";
    }

    public static Message fromJSON(String message) {
        try {
            JSONObject obj = (JSONObject)parser.parse(message);
            Message msg = new Message();
            //noinspection unchecked
            msg.setType((String)obj.get("type"));
            //noinspection unchecked
            msg.setValue((String)obj.get("value"));
            return msg;
        } catch (ParseException pex) {
            return null;
        }
    }

    public String toJSON() {
        JSONObject obj = new JSONObject();

        //noinspection unchecked
        obj.put("type", this.type);
        //noinspection unchecked
        obj.put("value", this.value);
        return obj.toJSONString();
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
