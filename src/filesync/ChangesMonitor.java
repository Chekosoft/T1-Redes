package filesync;

import cl.utfsm.redes.tarea1.client.ChangesSender;
import cl.utfsm.redes.tarea1.common.Message;

import java.io.File;
import java.io.IOException;
import java.nio.file.*;
import java.util.logging.Logger;


public class ChangesMonitor {
    private static final Logger Log = Logger.getLogger(ChangesMonitor.class.getName());

    private Path filePath;
    private String host;
    private int port;
    private int blockSize;

    public ChangesMonitor(String fileName, String host, int port, int blockSize) {
        this.filePath = Paths.get(new File(fileName).getAbsolutePath());
        this.host = host;
        this.port = port;
        this.blockSize = blockSize;
        Log.info("Absolute filepath: " + this.filePath);
        Log.info("Server located at: " + this.host);
        Log.info("Remote port: " + this.port);
        Log.info("Update Block size: " + this.blockSize);
    }

    public void startWatching() {
        SynchronisedFile synFile;
        ChangesSender sender;

        Log.info("Starting synchronization...");
        try { synFile = new SynchronisedFile(this.filePath.toString(), this.blockSize); }
        catch(IOException ioe) {
            Log.severe("Cannot start synchronized file: " + ioe.getMessage());
            return;
        }

        try {
            Log.info("Starting communication with server...");
            sender = new ChangesSender(this.host, this.port);
            sender.connect();
        } catch(IOException ioe) {
            Log.severe("Cannot connect to server specified in configuration.");
            return;
        }

        try {
            Message blockSizeMsg = new Message();
            blockSizeMsg.setType(Message.SET_BLOCKSIZE);
            blockSizeMsg.setValue(String.format("%d", this.blockSize));
            String response = sender.sendMessage(blockSizeMsg.toJSON());
            Message blockSizeResponse = Message.fromJSON(response);
            if(blockSizeResponse.getType().equals(Message.BYE_BYE)) {
                throw new Exception(blockSizeResponse.getValue());
            }

        } catch (IOException ioe){
            Log.severe("Could not send block size message to server: " + ioe.getMessage());
            return;
        } catch (Exception e) {
            Log.severe("Server answered with a Bye-Bye: " + e.getMessage());
            try { sender.close(); }
            catch(IOException ioe) { Log.warning("Could not disconnect, but it doesn't matter"); }
            return;
        }

        try {
            Instruction inst;
            synFile.CheckFileState();
            while(true) {
                if((inst = synFile.NextInstruction()) != null) {
                    Message instructionMessage = new Message();
                    instructionMessage.setType(Message.FILESYNC_WRAPPER);
                    instructionMessage.setValue(inst.ToJSON());
                    String instructionJSON = instructionMessage.toJSON();
                    Log.info(String.format("Sending %s",
                            instructionMessage.getType()));

                    String response = sender.sendMessage(instructionJSON);
                    Message msg = Message.fromJSON(response);
                    Log.info(String.format("Server sent %s response", msg.getType()));
                    if(msg.getType().equals(Message.BLOCK_NOT_FOUND)) {
                        Log.warning("Server wasnt aware of UpdateInstruction, sending as NewBlock");
                        Message newBlockMessage = new Message();
                        newBlockMessage.setType(Message.FILESYNC_WRAPPER);
                        NewBlockInstruction nb = new NewBlockInstruction((CopyBlockInstruction)inst);
                        newBlockMessage.setValue(nb.ToJSON());
                        Log.info(String.format("Sending %s:%s",
                                newBlockMessage.getType(),
                                newBlockMessage.getValue().substring(0, 8)));
                        response = sender.sendMessage(newBlockMessage.toJSON());
                        msg = Message.fromJSON(response);
                        Log.info(String.format("Server sent %s response", msg.getType()));
                    } else if(msg.getType().equals(Message.BYE_BYE)) {
                        Log.warning("BYE_BYE received, disconnecting at request of server");
                        sender.close();
                        break;
                    }

                    if(inst.Type().equals("EndUpdate")) {
                        Log.info("No more data in queue, rechecking file");
                        synFile.CheckFileState();
                    }
                }
            }
        } catch(IOException ioe) {
            Log.severe("Could not read synchonised file: " + ioe.getMessage());
        } catch(InterruptedException ie) {
            Log.severe("Execution was interrupted while checking file changes: " + ie.getMessage());
        }
    }
}
