package cl.utfsm.redes.tarea1.server;

import cl.utfsm.redes.tarea1.common.Message;
import filesync.BlockUnavailableException;
import filesync.ChangesReflector;
import org.kohsuke.args4j.Option;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.CmdLineException;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Logger;


public class ChangesServer {
    //PLIS
    private static final int defaultPort = 4715;
    private static final Logger Log= Logger.getLogger(ChangesServer.class.getName());

    @Option(name="-port", usage="Port which the server will listen to requests")
    private Integer port;
    @Option(name="-file", usage="Filename where the changes will be received", required=true)
    private String filename;

    private ChangesReflector reflector;

    public void startServer(String[] args) {
        CmdLineParser parser = new CmdLineParser(this);
        parser.getProperties().withUsageWidth(80);
        try {
            parser.parseArgument(args);
            Log.info("Writing changes to: " + this.filename);
            if(filename == null) throw new CmdLineException("File parameter was not defined.");
            if(port == null) this.port = ChangesServer.defaultPort;
        } catch (CmdLineException cmde) {
            Log.severe("Error retrieving options from command line: ");
            Log.severe(cmde.getMessage());
            Log.severe("Closing application, bye!");
            return;
        }
        Socket handlerSocket;
        ServerSocket serverSocket;
        try {
            serverSocket = new ServerSocket(this.port);
            while (true) {
                try {
                    Log.info("Waiting for client to connect");
                    handlerSocket = serverSocket.accept();
                    try {
                        BufferedReader socketReader = new BufferedReader(
                                new InputStreamReader(handlerSocket.getInputStream()));
                        DataOutputStream socketWriter = new DataOutputStream(handlerSocket.getOutputStream());
                        while(true) {
                            String inputMessage = socketReader.readLine();
                            if(inputMessage != null) {
                                Log.info("Processing... ");
                                String response = this.ProcessMessage(inputMessage);
                                if (response != null) {
                                    socketWriter.writeBytes(response + "\n");
                                }
                            } else {
                                Log.warning("Received null message, waiting for new client...");
                                handlerSocket.close();
                                break;
                            }
                        }
                    } catch(IOException ioe) {
                        Log.severe("The client unexpectedly hung up or an disconnection could not be made: " + ioe.getMessage());
                        Log.info("Restarting everything...");
                    }
                } catch(IOException ioe) {
                    Log.severe("Something happened while accepting client connection..." + ioe.getMessage());
                    Log.info("Restarting everything...");
                }
            }
        } catch (IOException ioe) {
            Log.severe("Error while creating server: " + ioe.getMessage());
        }
    }

    public String ProcessMessage(String input) throws IOException {
        Message msg = Message.fromJSON(input);
        String response = null;

        Log.info(String.format("Received message: %s", msg.getType()));

        if(msg.getType().equals(Message.FILESYNC_WRAPPER)) {
            try {
                String wrappedMessage = msg.getValue();
                reflector.processMessage(wrappedMessage);
                Message okMessage = new Message();
                okMessage.setType(Message.OK);
                response = okMessage.toJSON();
            } catch(IOException ex){
                Log.severe("An exception was found while updating files: " + ex.getMessage());
                Log.severe("Shutting down everything :(");
                Message bye_bye = new Message();
                bye_bye.setType(Message.BYE_BYE);
                bye_bye.setValue("Could not synchronise file: " + ex.getMessage());
                response = bye_bye.toJSON();
            } catch(BlockUnavailableException bex) {
                Log.warning("Received block from server not available, sending request to client.");
                Message errorMsg = new Message();
                errorMsg.setType(Message.BLOCK_NOT_FOUND);
                response = errorMsg.toJSON();
            }
        } else if(msg.getType().equals(Message.SET_BLOCKSIZE)) {
            try {
                Integer blockSize = Integer.parseInt(msg.getValue());
                reflector = new ChangesReflector(this.filename, blockSize);
                reflector.startWatching();
                Message okMessage = new Message();
                okMessage.setType(Message.OK);
                response = okMessage.toJSON();
            } catch(NumberFormatException fex) {
                Message bye_bye = new Message();
                bye_bye.setType(Message.BYE_BYE);
                bye_bye.setValue("Invalid blockSize value");
                response = bye_bye.toJSON();
            } catch(IOException ioe) {
                Message bye_bye = new Message();
                bye_bye.setType(Message.BYE_BYE);
                bye_bye.setValue(ioe.getMessage());
                response = bye_bye.toJSON();
            }
        }
        return response;
    }

    public static void main(String[] args) {
        ChangesServer cs = new ChangesServer();
        cs.startServer(args);
    }
}
