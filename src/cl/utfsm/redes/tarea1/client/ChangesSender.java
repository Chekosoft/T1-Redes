package cl.utfsm.redes.tarea1.client;

import java.io.*;
import java.net.Socket;
import java.util.logging.Logger;


public class ChangesSender {
    private static final Logger Log = Logger.getLogger(ChangesSender.class.getName());
    private Socket clientSocket;
    private DataOutputStream toServer;
    private BufferedReader fromServer;
    private String host;
    private int port;

    public ChangesSender(String host, int port) {
        this.host = host;
        this.port = port;
    }

    public void connect() throws IOException {
        Log.info(String.format("Connecting to server %s:%d", host, port));
        clientSocket = new Socket(host, port);
        toServer = new DataOutputStream(clientSocket.getOutputStream());
        fromServer = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
    }

    public String sendMessage(String message) throws IOException {
        toServer.writeBytes(message + "\n");
        return fromServer.readLine();
    }

    public void close() throws IOException {
        Log.info("Closing connection.");
        clientSocket.close();
    }

}
