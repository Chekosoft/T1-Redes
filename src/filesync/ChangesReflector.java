package filesync;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.logging.Logger;

public class ChangesReflector {
    private static final Logger Log = Logger.getLogger(ChangesReflector.class.toString());

    private SynchronisedFile synFile;
    private Path writePath;
    private InstructionFactory instFactory;
    private int blockSize;

    public ChangesReflector(String fileName, int blockSize) {
        this.writePath = Paths.get(new File(fileName).getAbsolutePath());
        this.blockSize = blockSize;
    }

    public void startWatching() throws IOException{
        Log.info(String.format("Start watching file %s", this.writePath.toString()));
        if(!this.writePath.toFile().exists() && !this.writePath.toFile().createNewFile()) {
            throw new IOException("Cannot open or create watch file " + this.writePath.toString());
        }
        this.synFile = new SynchronisedFile(this.writePath.toString(), this.blockSize);
        this.instFactory = new InstructionFactory();
    }

    public void processMessage(String message) throws BlockUnavailableException, IOException {
        Instruction inst = instFactory.FromJSON(message);
        this.synFile.ProcessInstruction(inst);
    }
}
