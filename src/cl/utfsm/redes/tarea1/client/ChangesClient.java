package cl.utfsm.redes.tarea1.client;

import filesync.ChangesMonitor;
import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.Option;

import java.util.logging.Logger;


public class ChangesClient {
    private static final Logger Log = Logger.getLogger(ChangesClient.class.getName());
    //PLIS
    private static final int defaultPort = 4715;
    private static final int defaultBlockSize = 1024;

    @Option(name="-file", usage="Archivo el que se desea sincronizar", required=true)
    private String fileName;

    @Option(name="-host", usage="Hostname al que se desea conectar", required=true)
    private String host;

    @Option(name="-port", usage="Define el puerto en que el host está escuchando")
    private Integer port;

    @Option(name="-blocksize", usage="Set synchronization block size")
    private Integer blockSize;

    public void startMonitoring(String[] args) {
        CmdLineParser parser = new CmdLineParser(this);
        parser.getProperties().withUsageWidth(80);
        try {
            parser.parseArgument(args);
            Log.info("Writer changes in: " + this.fileName);
            if(fileName == null) throw new CmdLineException("El archivo no fue definido.");
            if(port == null) this.port = ChangesClient.defaultPort;
            if(blockSize == null) this.blockSize = ChangesClient.defaultBlockSize;
        } catch (CmdLineException cmde){
            Log.severe("Error al obtener linea de comandos: ");
            Log.severe(cmde.getMessage());
            return;
        }

        ChangesMonitor mon = new ChangesMonitor(fileName, host, port, blockSize);
        mon.startWatching();
    }
    public static void main(String[] args) {
        ChangesClient clt = new ChangesClient();
        clt.startMonitoring(args);
    }
}
